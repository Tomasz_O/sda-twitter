package sda.twitter.twitter.model;

public enum RoleType {
    ROLE_ADMIN, ROLE_USER;
}
