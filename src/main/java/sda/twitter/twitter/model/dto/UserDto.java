package sda.twitter.twitter.model.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
    public class UserDto {
        private Long id;
        private String name;
        private String surname;
        private Date joinDate;
}
