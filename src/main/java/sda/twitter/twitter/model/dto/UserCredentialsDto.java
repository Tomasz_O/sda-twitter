package sda.twitter.twitter.model.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserCredentialsDto extends UserDto {
    private String emailAdress;
    private String password;
    private String role;
}
