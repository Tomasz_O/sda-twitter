package sda.twitter.twitter.model.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.Size;

@Entity
public class UserCredentials extends User {
    @Column(name = "USR_EMAIL")
    private String emailAdress;//this is login
    @Column(name = "USR_PASSWORD")
    //@Size(min = 5, max = 30, message = "password lenght should be between 5 and 20 characters.")
    private String password;
    @Column(name = "USR_ROLE")
    private String role;

    public String getEmailAdress() {
        return emailAdress;
    }

    public void setEmailAdress(String emailAdress) {
        this.emailAdress = emailAdress;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}

