package sda.twitter.twitter.service;


import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sda.twitter.twitter.model.dto.MessageDto;
import sda.twitter.twitter.model.entity.Message;
import sda.twitter.twitter.repository.MessageRepository;

import java.util.List;
import java.util.Optional;

@Service
public class MessageService {

    @Autowired
    ModelMapper mapper;

    @Autowired
    private MessageRepository messageRepository;

    public void save(MessageDto messageDto) {
        System.out.println("Save new message");
        Message message = mapper.map(messageDto, Message.class);
        messageRepository.save(message);
    }

    public Message findMessageById(Long id) {
        System.out.println("Find message by id" + messageRepository.findById(id).get().getId());
        Optional<Message> byId = messageRepository.findById(id);
        return byId.orElseGet(Message::new);
    }

    public void deleteMessage(MessageDto messageDto) {
        System.out.println("Delete message by id: " + messageDto.getId());
        messageRepository.deleteById(messageDto.getId());
    }

    public List<Message> findAll() {
        return messageRepository.findAll();
    }

    public void editMessage(Message messageDto) {
        System.out.println("Edit message ");
        Message message = findMessageById(messageDto.getId());
        message.setValue(messageDto.getValue());
        messageRepository.save(message);
    }

/*    private MessageDto setUpMessage(MessageDto messageDto) {
        MessageDto messageDtoNewValue = null;
        messageDtoNewValue.setValue(messageDto.getValue());
        return messageDtoNewValue;
    }*/
}
