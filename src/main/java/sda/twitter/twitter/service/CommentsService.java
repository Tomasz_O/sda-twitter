package sda.twitter.twitter.service;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sda.twitter.twitter.model.dto.CommentsDto;
import sda.twitter.twitter.model.entity.Comments;
import sda.twitter.twitter.repository.CommentsRepository;

import java.util.List;
import java.util.Optional;

@Service
public class CommentsService {

    @Autowired
    ModelMapper mapper;

    @Autowired
    private CommentsRepository commentsRepository;

    public void save(CommentsDto commentsDto) {
        Comments comments = mapper.map(commentsDto, Comments.class);
        commentsRepository.save(comments);
    }

    public Comments findCommentsById(Long id) {
        Optional<Comments> byId = commentsRepository.findById(id);
        return byId.orElseGet(Comments::new);
    }

    public void deleteComments(CommentsDto commentsDto) {
        commentsRepository.deleteById(commentsDto.getId());
    }

    public List<Comments> findAll() {
        return commentsRepository.findAll();
    }


}
