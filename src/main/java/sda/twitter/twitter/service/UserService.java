package sda.twitter.twitter.service;


import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import sda.twitter.twitter.model.dto.UserCredentialsDto;
import sda.twitter.twitter.model.dto.UserDto;
import sda.twitter.twitter.model.entity.User;
import sda.twitter.twitter.model.entity.UserCredentials;
import sda.twitter.twitter.repository.UserCredentialsRepository;
import sda.twitter.twitter.repository.UserRepository;
import sda.twitter.twitter.validation.BindingValidator;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class UserService {
    @Autowired
    ModelMapper mapper;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserCredentialsRepository userCredentialsRepository;

    @Autowired
    private PasswordEncoder bCryptPasswordEncoder;


/*    public void save(UserCredentialsDto userCredentialsDto) {
        UserCredentials userCredentials = mapper.map(userCredentialsDto, UserCredentials.class);
        //user.setJoinDate(new Date());
        userCredentialsRepository.save(userCredentials);
    }*/

    public void updateUser(User user) {
        userRepository.save(user);
    }

    public void delete(Long id) {
        userRepository.deleteById(id);
    }

    public List<User> getAllUsers() {
        return userRepository.findAll();
    }

    public void save(UserCredentialsDto userCredentialsDto, BindingResult result) {
        BindingValidator.validate(result);
        validateUserCredentials(userCredentialsDto);

        String hash = bCryptPasswordEncoder.encode(userCredentialsDto.getPassword());
        UserCredentials userCredentials = mapper.map(userCredentialsDto, UserCredentials.class);
        userCredentials.setPassword(hash);
        userCredentialsRepository.save(userCredentials);
    }

    private void validateUserCredentials(UserCredentialsDto userCredentialsDto) {
        if (userCredentialsExist(userCredentialsDto)) {
            throw new RuntimeException("Login already exist in database!");
        }
        if (!checkAuthorities(userCredentialsDto)){
            throw new RuntimeException("Wrong role");
        }
    }

    private boolean userCredentialsExist(UserCredentialsDto userCredentialsDto) {
        return userCredentialsRepository.countByEmailAdress(userCredentialsDto.getEmailAdress()) > 0;
    }

    private boolean checkAuthorities(UserCredentialsDto userCredentialsDto) {
        return SecurityContextHolder
                .getContext()
                .getAuthentication()
                .getAuthorities()
                .toArray()[0]
                .toString()
                .equals(userCredentialsDto.getRole());
    }

    public User findUserById(Long id) {
        Optional<User> byId = userRepository.findById(id);
        return byId.orElseGet(User::new);
    }


}
