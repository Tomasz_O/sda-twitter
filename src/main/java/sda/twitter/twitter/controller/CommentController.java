package sda.twitter.twitter.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.view.RedirectView;
import sda.twitter.twitter.model.dto.CommentsDto;
import sda.twitter.twitter.model.entity.Comments;
import sda.twitter.twitter.service.CommentsService;
import sda.twitter.twitter.service.MessageService;

@Controller
public class CommentController {

    @Autowired
    private CommentsService commentsService;

    @GetMapping("/comments")
    public String showAllComments(Model model) {
        model.addAttribute("allComments", commentsService.findAll());
        model.addAttribute("comments", new CommentsDto());
        return "comments";
    }

    @PostMapping("/comments/add")
    public String addNewMessage(CommentsDto commentsDto, Model model) {
        commentsService.save(commentsDto);
        model.addAttribute("allComments", commentsService.findAll());
        model.addAttribute("comments", new CommentsDto());
        return "comments";  //make direction to message-comments, doesnt work
    }

    @PostMapping("/deleteComments")
    public String deleteMessages(@ModelAttribute("comments") CommentsDto commentsDto, Model model) {
        commentsService.deleteComments(commentsDto);
        model.addAttribute("allComments", commentsService.findAll());
        model.addAttribute("comments", new CommentsDto());
        return "message-comments";
    }

    @GetMapping("/comments/edit/{id}")
    public String showUpdateForm(@PathVariable("id") long id, Model model) {
        Comments comments = commentsService.findCommentsById(id);
        model.addAttribute("comments", comments);
        return "comments-edit";
    }

    @PostMapping("/comments/update/{id}")
    public RedirectView updateMessage(@PathVariable("id") long id, CommentsDto commentsDto, Model model) {
        commentsDto.setId(id);
        commentsService.save(commentsDto);
        return new RedirectView("/comments");
    }

/*    @GetMapping("/message/comment/{id}")
    public String commentMessage(@PathVariable("id") long id, Model model) {
        Comments comments = commentsService.findCommentsById(id);
        model.addAttribute("comments", comments);
        return "message-comments";
    }*/
}
