package sda.twitter.twitter.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.view.RedirectView;
import sda.twitter.twitter.model.dto.CommentsDto;
import sda.twitter.twitter.model.dto.MessageDto;
import sda.twitter.twitter.model.entity.Message;
import sda.twitter.twitter.service.MessageService;

@Controller
public class MessageController {

    @Autowired
    private MessageService messageService;


    @GetMapping("/message")
    public String showAllMessages(Model model) {
        model.addAttribute("allMessages", messageService.findAll());
        model.addAttribute("message", new MessageDto());
        return "message";
    }

    @PostMapping("/message/add")
    public String addNewMessage(MessageDto messageDto, Model model) {
        messageService.save(messageDto);
        model.addAttribute("allMessages", messageService.findAll());
        model.addAttribute("message", new MessageDto());
        return "message";
    }

    @PostMapping("/deleteMessage")
    public String deleteMessages(@ModelAttribute("message") MessageDto messageDto, Model model) {
        messageService.deleteMessage(messageDto);
        model.addAttribute("allMessages", messageService.findAll());
        model.addAttribute("message", new MessageDto());
        return "message";
    }

    @GetMapping("/message/edit/{id}")
    public String showUpdateForm(@PathVariable("id") long id, Model model) {
        Message message = messageService.findMessageById(id);
        model.addAttribute("message", message);
        return "message-edit";
    }

    @PostMapping("/message/update/{id}")
    public RedirectView updateMessage(@PathVariable("id") long id, MessageDto messageDto, Model model) {
        messageDto.setId(id);
        messageService.save(messageDto);
        return new RedirectView("/message");
    }

    @GetMapping("/message/{id}/comment")
    public RedirectView commentMessage(@PathVariable("id") long id, Model model) {
        return new RedirectView("/message-comments?id="+id);
    }
    @GetMapping("/message-comments")
    public String test(@RequestParam("id") Long id, Model model) {
        Message message = messageService.findMessageById(id);
        model.addAttribute("message", message);

        model.addAttribute("comments", new CommentsDto());
        return "message-comments";
    }
}
