package sda.twitter.twitter.controller;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.view.RedirectView;
import sda.twitter.twitter.model.dto.UserCredentialsDto;
import sda.twitter.twitter.model.dto.UserDto;
import sda.twitter.twitter.model.entity.User;
import sda.twitter.twitter.service.UserService;

@Controller
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("/users")
    public String getAllUsers(Model model) {
        model.addAttribute("allUsers", userService.getAllUsers());
        return "users";
    }

    @GetMapping("/user/add")
    public String getUser(Model model) {
        model.addAttribute("user", new UserCredentialsDto());
        return "add-user";
    }

    @PostMapping("/user/add")
    public RedirectView addUser(@ModelAttribute("user") UserCredentialsDto userCredentialsDto, Model model, BindingResult result) {
        model.addAttribute("allUsers", userService.getAllUsers());
        userService.save(userCredentialsDto, result);
        return new RedirectView("/users");
    }

    @GetMapping("/user/edit/{id}")
    public String showUpdateForm(@PathVariable("id") long id, Model model) {
        User user = userService.findUserById(id);
        model.addAttribute("user", user);
        return "edit-user";
    }

    @PostMapping("/user/update/{id}")
    public RedirectView updateUser(@PathVariable("id") long id, UserCredentialsDto userCredentialsDto , BindingResult result) {
        userCredentialsDto.setId(id);
        userService.save(userCredentialsDto, result);
//        userService.updateUser(user);
        return new RedirectView("/users");
    }

    @GetMapping("/user/delete/{id}")
    public RedirectView deleteUser(@PathVariable("id") long id, Model model) {
        userService.delete(userService.findUserById(id).getId());
        model.addAttribute("users", userService.getAllUsers());
        return new RedirectView("/users");
    }
}

