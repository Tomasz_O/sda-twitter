package sda.twitter.twitter.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import sda.twitter.twitter.model.dto.MessageDto;
import sda.twitter.twitter.model.dto.UserDto;

@Controller
public class IndexController {
    @RequestMapping(value = {"/index"})
    public String index() {
        return "index";
    }


    @GetMapping("/index/login")
    public String showLoginPage(Model model) {
        model.addAttribute("user", new UserDto());
        return "login";
    }

    @GetMapping("/index/register")
    public String showRegisterPage(Model model) {
        model.addAttribute("user", new UserDto());
        return "add-user";
    }

    @GetMapping("/index/message")
    public String showMessagePage(Model model) {
        model.addAttribute("message", new MessageDto());
        return "message";
    }

}
