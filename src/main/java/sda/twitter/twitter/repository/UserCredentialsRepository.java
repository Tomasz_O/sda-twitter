package sda.twitter.twitter.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import sda.twitter.twitter.model.entity.User;
import sda.twitter.twitter.model.entity.UserCredentials;

public interface UserCredentialsRepository extends JpaRepository<UserCredentials, Long> {
    Long countByEmailAdress(String email);
}