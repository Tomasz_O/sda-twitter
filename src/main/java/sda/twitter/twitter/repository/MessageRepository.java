package sda.twitter.twitter.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import sda.twitter.twitter.model.entity.Message;

public interface MessageRepository extends JpaRepository<Message, Long> {
}
