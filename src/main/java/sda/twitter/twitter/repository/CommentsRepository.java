package sda.twitter.twitter.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import sda.twitter.twitter.model.entity.Comments;



public interface CommentsRepository extends JpaRepository<Comments, Long> {
}
